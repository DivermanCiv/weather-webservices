import os
from dotenv import load_dotenv
import web
import requests
import time
import json
load_dotenv()
API_KEY = os.getenv('API_KEY')
urls = (
    '/weather', 'weather',
)

app = web.application(urls, globals())


class weather:

    def url_constructor(zipcode, countryCode, units):
        """this method builds and returns an url used to make a request to openweathermap API"""
        base_url = 'http://api.openweathermap.org/data/2.5/weather'
        zipcode = '?zip='+zipcode
        country = ','+countryCode
        units = '&units='+units
        url = base_url+zipcode+country+units+'&appid=' + API_KEY
        return url

    def GET(self):
        """This method catch the zipcode put in the URL and send a request to the openweathermap API to get the weather and temperatures of the requested location"""
        web.header('Content-Type', 'text/html; charset=utf-8', unique=True)
        data = web.input()
        zc = data.zipcode
        url = weather.url_constructor(zc, 'fr', 'metric')

        #checkDatafile(url)

        response = requests.post(url)
        jsonObj = response.json()
        if jsonObj['cod'] != 200:
            return weather.zipcodeNotFound()
        else:
            actualTemp = str(jsonObj['main']['temp'])
            minTemp = str(jsonObj['main']['temp_min'])
            maxTemp = str(jsonObj['main']['temp_max'])
            actualWeather = str(jsonObj['weather'][0]['main'])
            cityName = str(jsonObj['name'])

            weather.putDatafile(url,cityName, actualTemp, minTemp, maxTemp, actualWeather)
            return 'Temperature at '+cityName+':\nActual temperature : '+actualTemp+' \nMin temperature : '+minTemp+' \nMax temperature : '+maxTemp+' \nMeteo : '+actualWeather


    @staticmethod
    def putDatafile(url, cityName, actualTemp, minTemp, maxTemp, actualWeather ):
        """This method takes the informations returned by the API and stock them in a txt file with a timestamp"""

        timeStamp = str(time.time())
        f = open("weatherdata.txt", 'a+')
        f.write('{"URL":"'+url+'","cityName":"'+cityName+'","actualTemp":"'+actualTemp+'","minTemp":"'+minTemp+'","maxTemp":"'+maxTemp+'","actualWeather":"'+actualWeather+'","timeStamp":"'+timeStamp+'"},')
        f.close()
        return None

    @staticmethod
    def checkDatafile(url):
        """This method checks if the url to use for the API request has already been used in the last 15min. If true, it returns the last informations collected about this URL. Else, it does nothing"""

        ############################Work in progress#############################

        timeStamp = time.time()
        f = open("weatherdata.txt", 'r+')
        content = f.read()
        dictionnary = json.loads(content)
        print(type(dictionnary))
        f.close()
        return None

    @staticmethod
    def zipcodeNotFound():
        """this method is called when the response code from the API is different than 200"""
        return 'Sorry, this zipcode doesn\'t seem to exist'


if __name__  == "__main__":
    app.run()